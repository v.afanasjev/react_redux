export const getIdFromString = (str: string) => str.replace(/^\D+/g, '');

export const getPostsRequestPath = (userId: string, offset: number, limit: number) =>
    `/posts/${userId}?offset=${offset}&limit=${limit}`;

export const encodingValues = (values: any) => {
    return Object.entries(values).reduce((encode: typeof values, [key, val]) => {
        encode[key as keyof typeof values] = encodeURI(String(val));
        return encode;
    }, {});
};
