import { localStorageKeys } from '../common-constants';

export const saveToken = (token: string) => {
    localStorage.setItem(localStorageKeys.accessToken, token);
};
export const saveUserId = (userId: string) => {
    localStorage.setItem(localStorageKeys.loggedInUserId, userId);
};

export const getToken = () => localStorage.getItem(localStorageKeys.accessToken) ?? '';

export const getLoggedInUserId = () => localStorage.getItem(localStorageKeys.loggedInUserId) ?? '';

export const logout = () => {
    localStorage.clear();
};
