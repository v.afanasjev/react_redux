import { forms, modes, postsPerPage } from '../common-constants';
import type { TModalMode } from '../slices/modal-slice';

const { createPost, editPost } = modes;

export const dateFormatter = (date: string) => new Date(date).toLocaleString();

export const getPageCount = (total: number) => Math.ceil(total / postsPerPage);

export const getAvatarSrc = (path: string | null) =>
    path ? `${process.env?.REACT_APP_BASE_URL?.toString()}${path}` : '';

export const checkIsModalWindowLarge = (mode: TModalMode) =>
    mode === createPost || mode.includes(editPost);

export const getCurrentFormName = (mode: TModalMode) => {
    if (mode.includes(editPost) || mode === createPost) {
        return forms.createEditPost.formName;
    }
    return forms.editProfile.formName;
};
