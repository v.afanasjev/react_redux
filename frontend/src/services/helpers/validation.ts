import { capitalize } from '@mui/material';

const removeExtraSpaces = (str: string, i = 0, res = ''): string => {
    if (i >= str.length) return res;
    if (str[i] === ' ' && str[i + 1] === ' ') {
        return removeExtraSpaces(str, i + 1, res);
    }
    const result = res + str[i];
    return removeExtraSpaces(str, i + 1, result);
};

export const sanitize = (str: string) => removeExtraSpaces(str);

export const sanitizeWithCapitalize = (str: string) => capitalize(removeExtraSpaces(str));

export const formValidate = (formFields: Record<string, string>) => (values: typeof formFields) => {
    const errors = {} as unknown as typeof formFields;

    Object.keys(formFields).forEach((key) => {
        const value = values?.[key];
        if (!value) {
            errors[key] = 'Required';
        } else if (value.length < 4) {
            errors[key] = 'Must be more than 4 characters';
        }
    });

    return errors;
};
