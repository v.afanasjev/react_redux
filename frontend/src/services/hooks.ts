import { useEffect, useRef } from 'react';
import {
    useDispatch as useDispatchIn,
    useSelector as useSelectorIn,
    TypedUseSelectorHook,
} from 'react-redux';
import { type TApplicationAction } from './types/application-action';
import { type Dispatch } from '@reduxjs/toolkit';
import { type TApplicationState } from './types/application-state';

export const useDispatch = () => useDispatchIn<Dispatch<TApplicationAction>>();
export const useSelector: TypedUseSelectorHook<TApplicationState> = useSelectorIn;

export const useOnLoadPage = (action: TApplicationAction | Array<TApplicationAction>) => {
    const dispatch = useDispatch();
    const dispatchRef = useRef(dispatch);
    const actionRef = useRef(action);
    useEffect(() => {
        if (Array.isArray(actionRef.current)) {
            actionRef.current.forEach((act) => dispatchRef.current(act));
        } else dispatchRef.current(actionRef.current);
    }, []);
};
