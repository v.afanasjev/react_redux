import { getIdFromString } from './helpers/prepare-to-request-helpers';
import { LOGIN_PAGE, USER_PAGE } from './slices/constants';
import type { TApplicationState } from './types/application-state';

export const loadingSelector = (state: TApplicationState) => state.loading;

export const usersDataSelector = (state: TApplicationState) => state.users;

export const pagesSelector = (state: TApplicationState) => state.pages;

export const postsDataSelector = (state: TApplicationState) => state.posts;

export const modalSelector = (state: TApplicationState) => state.modal;

export const getUserIdSelector = (state: TApplicationState) =>
    getIdFromString(pagesSelector(state).current);

export const formSelector = (formName: string) => (state: TApplicationState) =>
    state.form[formName];

export const getPostIdSelector = (state: TApplicationState) =>
    getIdFromString(modalSelector(state).mode);

export const authSelector = (state: TApplicationState) => state.auth;

export const isLoginPageSelector = (state: TApplicationState) => {
    const currentPage = pagesSelector(state).current;
    return currentPage === LOGIN_PAGE;
};

export const isUserPageSelector = (state: TApplicationState) => {
    const currentPage = pagesSelector(state).current;
    return currentPage.includes(USER_PAGE);
};
