import { TPost } from './posts-types';
import { TUser } from './users-types';

export type TGetAllUsersResponseData = Readonly<{
    users: ReadonlyArray<TUser>;
}>;

export type TGetUserResponseData = Readonly<{
    user: TUser;
}>;

export type TGetPostsResponseData = Readonly<{
    posts: ReadonlyArray<TPost>;
}>;

export type TCommonResponse = Readonly<{
    success: boolean;
}>;

export type TLoginResponse = Readonly<{
    success: boolean;
    token: string;
    userId: string;
}>;
