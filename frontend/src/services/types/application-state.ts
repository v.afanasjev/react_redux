import type { FormStateMap } from 'redux-form/lib/reducer';
import type {
    authSliceName,
    loadingSliceName,
    modalSliceName,
    pagesSliceName,
    postsSliceName,
    usersSliceName,
} from '../slices/constants';
import type { TModalState } from '../slices/modal-slice';
import type { TPagesState } from '../slices/pages-slice';
import type { TPostsState } from '../slices/posts-slice';
import type { TUsersState } from '../slices/users-slice';

export type TApplicationState = Readonly<{
    form: FormStateMap;
    [pagesSliceName]: TPagesState;
    [usersSliceName]: TUsersState;
    [postsSliceName]: TPostsState;
    [loadingSliceName]: boolean;
    [modalSliceName]: TModalState;
    [authSliceName]: string;
}>;
