import type { TSagasActions } from '../sagas/actions';
import type { TSetUserAuthAction } from '../slices/auth-slice';
import type { TSetLoadingAction } from '../slices/loading-slice';
import type { TModalActions } from '../slices/modal-slice';
import type { TPagesActions } from '../slices/pages-slice';
import type { TPostsActions } from '../slices/posts-slice';

export type TApplicationAction =
    | TPagesActions
    | TSagasActions
    | TSetLoadingAction
    | TPostsActions
    | TModalActions
    | TSetUserAuthAction;
