import { type TPost } from './posts-types';

export type TUser = Readonly<{
    email: string;
    password: string;
    profile_picture: string | null;
    user_id: number;
    username: string;
    postsTotal: number;
    posts?: ReadonlyArray<TPost>;
}>;
