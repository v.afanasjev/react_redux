export type TPost = Readonly<{
    post_id: number;
    user_id: number;
    title: string;
    content: string;
    createdAt: string;
    updatedAt: string;
}>;
