export const MAIN__PAGE_ON_LOADED = 'MAIN__PAGE_ON_LOADED';
export const USER__PAGE_ON_LOADED = 'USER__PAGE_ON_LOADED';

export const REQUESTS__GET_POSTS = 'REQUESTS__GET_POSTS';
export const REQUESTS__ADD_POST = 'REQUESTS__ADD_POST';
export const REQUESTS__EDIT_POST = 'REQUESTS__EDIT_POST';
export const REQUESTS__DELETE_POST = 'REQUESTS__DELETE_POST';

export const REQUESTS__REGISTRATION = 'REQUESTS__REGISTRATION';
export const REQUESTS__LOGIN = 'REQUESTS__LOGIN';
export const REQUESTS__EDIT_PROFILE = 'REQUESTS__EDIT_PROFILE';

export const ON_CHANGE_PAGE = 'ON_CHANGE_PAGE';
export const GET_POSTS__SUCCESS = 'GET_POSTS__SUCCESS';

export const FORM_SUBMIT = 'FORM_SUBMIT';
