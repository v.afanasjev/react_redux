import type { SagaIterator } from 'redux-saga';
import { takeEvery, put } from 'redux-saga/effects';
import { postsPerPage } from '../common-constants';
import { getUserIdSelector, postsDataSelector } from '../selectors';
import { setPage } from '../slices/posts-slice';
import { selectTs } from '../utils/redux-saga-effects';
import { getPosts, type TGetPostsSuccessAction, type TOnChangePageAction } from './actions';
import { GET_POSTS__SUCCESS, ON_CHANGE_PAGE } from './constants';

function* onChangePageWorker(action: TOnChangePageAction): SagaIterator {
    const userId = yield* selectTs(getUserIdSelector);
    const offset = postsPerPage * (action.payload.page - 1);
    const getPostsAction = getPosts({ userId, offset, limit: postsPerPage });
    yield put(getPostsAction);
}

function* getPostsSuccessWorker(action: TGetPostsSuccessAction): SagaIterator {
    const offset = action.payload.offset;
    if (offset) {
        yield put(setPage({ page: offset / postsPerPage + 1, offset }));
    } else {
        const page = yield* selectTs((state) => postsDataSelector(state).page);
        if (page !== 1) {
            yield put(setPage({ page: 1 }));
        }
    }
}

export function* pagesSaga(): SagaIterator {
    yield takeEvery(ON_CHANGE_PAGE, onChangePageWorker);
    yield takeEvery(GET_POSTS__SUCCESS, getPostsSuccessWorker);
}
