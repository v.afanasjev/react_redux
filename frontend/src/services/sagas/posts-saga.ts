import { getFormValues } from 'redux-form';
import type { SagaIterator } from 'redux-saga';
import { takeEvery, put, call } from 'redux-saga/effects';
import { forms } from '../common-constants';
import { encodingValues } from '../helpers/prepare-to-request-helpers';
import {
    addPostRequest,
    deletePostRequest,
    editPostRequest,
    getPostsRequest,
} from '../requests/posts-requests';
import { getPostIdSelector } from '../selectors';
import { toggleModal } from '../slices/modal-slice';
import { setPosts } from '../slices/posts-slice';
import { TGetPostsResponseData, TCommonResponse } from '../types/requests-types';
import { selectTs } from '../utils/redux-saga-effects';
import { requestWorker } from '../utils/request-worker';
import { getPostsSuccess, TGetPostsRequestAction } from './actions';
import {
    REQUESTS__ADD_POST,
    REQUESTS__DELETE_POST,
    REQUESTS__EDIT_POST,
    REQUESTS__GET_POSTS,
    USER__PAGE_ON_LOADED,
} from './constants';

const { formName, formFields } = forms.createEditPost;
const errorTopic = 'Posts';

export function* getPostsRequestWorker(action: TGetPostsRequestAction): SagaIterator {
    const { userId, offset, limit } = action.payload;
    const data: TGetPostsResponseData = yield call(
        requestWorker,
        () => getPostsRequest(userId, offset, limit),
        errorTopic
    );
    if (data) {
        yield put(getPostsSuccess({ offset }));
        yield put(setPosts(data.posts));
    }
}

export function* addPostRequestWorker(): SagaIterator {
    const values = yield* selectTs((state) => getFormValues(formName)(state) as typeof formFields);
    yield put(toggleModal(false));
    const data: TCommonResponse = yield call(
        requestWorker,
        () => addPostRequest(encodingValues(values)),
        errorTopic
    );
    if (data?.success) {
        yield put({ type: USER__PAGE_ON_LOADED });
    }
}

export function* editPostRequestWorker(): SagaIterator {
    const postId = yield* selectTs(getPostIdSelector);
    const values = yield* selectTs((state) => getFormValues(formName)(state) as typeof formFields);
    yield put(toggleModal(false));
    const data: TCommonResponse = yield call(
        requestWorker,
        () => editPostRequest(postId, encodingValues(values)),
        errorTopic
    );
    if (data?.success) {
        yield put({ type: USER__PAGE_ON_LOADED });
    }
}

export function* deletePostRequestWorker(): SagaIterator {
    const postId = yield* selectTs(getPostIdSelector);
    yield put(toggleModal(false));
    const data: TCommonResponse = yield call(
        requestWorker,
        () => deletePostRequest(postId),
        errorTopic
    );
    if (data?.success) {
        yield put({ type: USER__PAGE_ON_LOADED });
    }
}

export function* postsSaga(): SagaIterator {
    yield takeEvery(REQUESTS__GET_POSTS, getPostsRequestWorker);
    yield takeEvery(REQUESTS__ADD_POST, addPostRequestWorker);
    yield takeEvery(REQUESTS__EDIT_POST, editPostRequestWorker);
    yield takeEvery(REQUESTS__DELETE_POST, deletePostRequestWorker);
}
