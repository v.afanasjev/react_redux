import type { SagaIterator } from 'redux-saga';
import { takeEvery, put } from 'redux-saga/effects';

import { modes } from '../common-constants';
import { modalSelector, pagesSelector } from '../selectors';
import { selectTs } from '../utils/redux-saga-effects';
import { FORM_SUBMIT } from './constants';
import { addPost, deletePost, edifProfile, editPost, regProfile, userLogin } from './actions';
import { LOGIN_PAGE, REG_PAGE } from '../slices/constants';

export function* formSubmitWorker(): SagaIterator {
    const currentPage = yield* selectTs((state) => pagesSelector(state).current);
    if (currentPage === REG_PAGE) {
        yield put(regProfile());
        return;
    }
    if (currentPage === LOGIN_PAGE) {
        yield put(userLogin());
        return;
    }
    const mode = yield* selectTs((state) => modalSelector(state).mode);
    if (mode === modes.editProfile) {
        yield put(edifProfile());
    }
    if (mode === modes.createPost) {
        yield put(addPost());
    }
    if (mode.includes(modes.editPost)) {
        yield put(editPost());
    }
    if (mode.includes(modes.deletePost)) {
        yield put(deletePost());
    }
}

export function* formsSaga(): SagaIterator {
    yield takeEvery(FORM_SUBMIT, formSubmitWorker);
}
