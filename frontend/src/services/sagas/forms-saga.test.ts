import { expectSaga } from 'redux-saga-test-plan';
import { formSubmitWorker } from './forms-saga';
import { LOGIN_PAGE, REG_PAGE } from '../slices/constants';
import { modes } from '../common-constants';

describe('forms saga', () => {
    test('registattion page', () =>
        expectSaga(formSubmitWorker)
            .withState({ pages: { current: REG_PAGE } })
            .put({ type: 'REQUESTS__REGISTRATION', payload: undefined })
            .run());

    test('login page', () =>
        expectSaga(formSubmitWorker)
            .withState({ pages: { current: LOGIN_PAGE } })
            .put({ type: 'REQUESTS__LOGIN', payload: undefined })
            .run());

    test('user page with mode edit-profile', () =>
        expectSaga(formSubmitWorker)
            .withState({ pages: { current: '' }, modal: { mode: modes.editProfile } })
            .put({ type: 'REQUESTS__EDIT_PROFILE', payload: undefined })
            .run());

    test('user page with mode create-post', () =>
        expectSaga(formSubmitWorker)
            .withState({ pages: { current: '' }, modal: { mode: modes.createPost } })
            .put({ type: 'REQUESTS__ADD_POST', payload: undefined })
            .run());

    test('user page with mode edit-post', () =>
        expectSaga(formSubmitWorker)
            .withState({ pages: { current: '' }, modal: { mode: modes.editPost } })
            .put({ type: 'REQUESTS__EDIT_POST', payload: undefined })
            .run());

    test('user page with mode delete-post', () =>
        expectSaga(formSubmitWorker)
            .withState({ pages: { current: '' }, modal: { mode: modes.deletePost } })
            .put({ type: 'REQUESTS__DELETE_POST', payload: undefined })
            .run());
});
