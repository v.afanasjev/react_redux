import type { SagaIterator } from 'redux-saga';
import { takeEvery, call, put } from 'redux-saga/effects';
import { selectTs } from '../utils/redux-saga-effects';
import { REQUESTS__LOGIN, REQUESTS__REGISTRATION } from './constants';
import { notificationSuccess } from '../utils/notification';
import { loginRequest, registrationRequest } from '../requests/auth-requests';
import { forms } from '../common-constants';
import { getFormValues } from 'redux-form';
import { encodingValues } from '../helpers/prepare-to-request-helpers';
import { setTargetPage } from '../slices/pages-slice';
import { LOGIN_PAGE } from '../slices/constants';
import { logout, saveToken, saveUserId } from '../helpers/auth-helpers';
import { setUserAuth } from '../slices/auth-slice';
import { authSelector } from '../selectors';
import { requestWorker } from '../utils/request-worker';
import { TCommonResponse, TLoginResponse } from '../types/requests-types';

const { formName, formFields } = forms.auth;

const successMessage = 'User successfully registered';
const errorTopic = 'Auth';

function* registrationWorker(): SagaIterator {
    const values = yield* selectTs((state) => getFormValues(formName)(state) as typeof formFields);
    const data: TCommonResponse = yield call(
        requestWorker,
        () => registrationRequest(encodingValues(values)),
        errorTopic
    );
    if (data) {
        yield call(notificationSuccess, successMessage);
        yield put(setTargetPage(LOGIN_PAGE));
    }
}

function* loginWorker(): SagaIterator {
    const values = yield* selectTs((state) => getFormValues(formName)(state) as typeof formFields);
    const data: TLoginResponse = yield call(
        requestWorker,
        () => loginRequest(encodingValues(values)),
        errorTopic
    );
    if (data) {
        yield put(setTargetPage(`user/${data.userId}`));
        saveToken(data.token);
        yield put(setUserAuth(data.userId));
    }
}

function* loginLogoutUserWorker(): SagaIterator {
    const loggedInUserId = yield* selectTs(authSelector);
    if (loggedInUserId) {
        saveUserId(loggedInUserId);
    } else {
        logout();
    }
}

export function* authSaga(): SagaIterator {
    yield takeEvery(REQUESTS__REGISTRATION, registrationWorker);
    yield takeEvery(REQUESTS__LOGIN, loginWorker);
    yield takeEvery('auth/setUserAuth', loginLogoutUserWorker);
}
