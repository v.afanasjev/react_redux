import type { SagaIterator } from 'redux-saga';
import { takeEvery, call, put } from 'redux-saga/effects';
import {
    getAllUsersRequest,
    getOneUserRequest,
    updateUserProfileRequest,
} from '../requests/users-requests';
import { getUserIdSelector } from '../selectors';
import { setUsers } from '../slices/users-slice';
import { setTotal } from '../slices/posts-slice';
import { selectTs } from '../utils/redux-saga-effects';
import { getPosts } from './actions';
import { MAIN__PAGE_ON_LOADED, REQUESTS__EDIT_PROFILE, USER__PAGE_ON_LOADED } from './constants';
import { TGetAllUsersResponseData, TGetUserResponseData } from '../types/requests-types';
import { forms, postsPerPage } from '../common-constants';
import { getFormValues } from 'redux-form';
import { formDataController } from '../utils/form-data-controller';
import { toggleModal } from '../slices/modal-slice';
import { requestWorker } from '../utils/request-worker';

const { formName, formFields } = forms.editProfile;
const errorTopic = 'Users';

function* getUsersRequestWorker(): SagaIterator {
    const data: TGetAllUsersResponseData = yield call(
        requestWorker,
        () => getAllUsersRequest(),
        errorTopic
    );
    if (data) {
        yield put(setUsers(data.users));
    }
}

function* getUserRequestWorker(): SagaIterator {
    const userId = yield* selectTs(getUserIdSelector);
    const data: TGetUserResponseData = yield call(
        requestWorker,
        () => getOneUserRequest(userId),
        errorTopic
    );
    if (data) {
        const getPostsAction = getPosts({ userId, offset: 0, limit: postsPerPage });
        yield put(getPostsAction);
        yield put(setUsers(Array.of(data.user)));
        yield put(setTotal({ total: data.user.postsTotal }));
    }
}

function* editProfileFormSubmitWorker(): SagaIterator {
    const userId = yield* selectTs(getUserIdSelector);
    const values = yield* selectTs((state) => getFormValues(formName)(state) as typeof formFields);
    const formData: FormData = yield call(formDataController.getOrCreateFormData);
    formData.append(formFields.username, values.username);
    yield put(toggleModal(false));
    const data: TGetUserResponseData = yield call(
        requestWorker,
        () => updateUserProfileRequest(userId, formData),
        errorTopic
    );
    if (data) {
        yield put(setUsers(Array.of(data.user)));
    }
}

export function* usersSaga(): SagaIterator {
    yield takeEvery(MAIN__PAGE_ON_LOADED, getUsersRequestWorker);
    yield takeEvery(USER__PAGE_ON_LOADED, getUserRequestWorker);
    yield takeEvery(REQUESTS__EDIT_PROFILE, editProfileFormSubmitWorker);
}
