import type { SagaIterator } from 'redux-saga';
import { all, fork } from 'redux-saga/effects';
import { authSaga } from './auth-saga';
import { formsSaga } from './forms-saga';
import { pagesSaga } from './pages-saga';
import { postsSaga } from './posts-saga';
import { usersSaga } from './users-saga';

export function* rootSaga(): SagaIterator {
    yield all([fork(usersSaga), fork(postsSaga), fork(formsSaga), fork(pagesSaga), fork(authSaga)]);
}
