import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { requestWorker } from '../utils/request-worker';
import { TGetPostsRequestAction } from './actions';
import { getPostsRequestWorker, addPostRequestWorker } from './posts-saga';

describe('posts sagas', () => {
    describe('getPostsRequestWorker', () => {
        const action = {
            payload: { userId: '1', offset: 0, limit: 50 },
        } as TGetPostsRequestAction;
        test('request success', () => {
            const responseData = { posts: ['post1', 'post2'] };
            return expectSaga(getPostsRequestWorker, action)
                .provide([[matchers.call.fn(requestWorker), responseData]])
                .put({ type: 'GET_POSTS__SUCCESS', payload: { offset: 0 } })
                .put({ type: 'posts/setPosts', payload: responseData.posts })
                .run();
        });

        test('request fail', () => {
            const responseData = undefined;
            return expectSaga(getPostsRequestWorker, action)
                .provide([[matchers.call.fn(requestWorker), responseData]])
                .run();
        });
    });

    describe('addPostRequestWorker', () => {
        test('request success', () => {
            const responseData = { success: true };
            return expectSaga(addPostRequestWorker)
                .put({ type: 'modal/toggleModal', payload: false })
                .provide([[matchers.call.fn(requestWorker), responseData]])
                .put({ type: 'USER__PAGE_ON_LOADED' })
                .run();
        });

        test('request fail', () => {
            const responseData = undefined;
            return expectSaga(addPostRequestWorker)
                .put({ type: 'modal/toggleModal', payload: false })
                .provide([[matchers.call.fn(requestWorker), responseData]])
                .run();
        });
    });
});
