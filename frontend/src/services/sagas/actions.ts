import { createAction } from '@reduxjs/toolkit';
import {
    FORM_SUBMIT,
    GET_POSTS__SUCCESS,
    MAIN__PAGE_ON_LOADED,
    ON_CHANGE_PAGE,
    REQUESTS__ADD_POST,
    REQUESTS__EDIT_PROFILE,
    REQUESTS__GET_POSTS,
    USER__PAGE_ON_LOADED,
    REQUESTS__EDIT_POST,
    REQUESTS__DELETE_POST,
    REQUESTS__REGISTRATION,
    REQUESTS__LOGIN,
} from './constants';

// pages actions
export const mainPageOnLoaded = createAction(MAIN__PAGE_ON_LOADED);
export const userPageOnLoaded = createAction(USER__PAGE_ON_LOADED);
export const changePage = createAction<{ page: number }>(ON_CHANGE_PAGE);
export const getPostsSuccess = createAction<{ offset: number }>(GET_POSTS__SUCCESS);

type TMainPageOnLoadedAction = Pick<typeof mainPageOnLoaded, 'type'>;
type TUserPageOnLoadedAction = Pick<typeof userPageOnLoaded, 'type'>;
export type TOnChangePageAction = Pick<typeof changePage, 'type'> & { payload: { page: number } };
export type TGetPostsSuccessAction = Pick<typeof getPostsSuccess, 'type'> & {
    payload: { offset: number };
};

// posts actions
interface TGetPostsRequestsPayload {
    userId: string;
    offset: number;
    limit: number;
}
export const getPosts = createAction<TGetPostsRequestsPayload>(REQUESTS__GET_POSTS);
export const addPost = createAction(REQUESTS__ADD_POST);
export const editPost = createAction(REQUESTS__EDIT_POST);
export const deletePost = createAction(REQUESTS__DELETE_POST);

export type TGetPostsRequestAction = Pick<typeof getPosts, 'type'> & {
    payload: TGetPostsRequestsPayload;
};
type TAddPostRequestAction = Pick<typeof addPost, 'type'>;
type TEditPostRequestAction = Pick<typeof editPost, 'type'>;
type TDeletePostRequestAction = Pick<typeof deletePost, 'type'>;

// users actions
export const regProfile = createAction(REQUESTS__REGISTRATION);
export const userLogin = createAction(REQUESTS__LOGIN);
export const edifProfile = createAction(REQUESTS__EDIT_PROFILE);

type TRegProfileAction = Pick<typeof regProfile, 'type'>;
type TUserLoginAction = Pick<typeof userLogin, 'type'>;
type TEditProfileAction = Pick<typeof edifProfile, 'type'>;

// form actions
export const formSubmit = createAction(FORM_SUBMIT);

export type TFormSubmitAction = Pick<typeof formSubmit, 'type'>;

export type TSagasActions =
    | TMainPageOnLoadedAction
    | TUserPageOnLoadedAction
    | TGetPostsRequestAction
    | TOnChangePageAction
    | TGetPostsSuccessAction
    | TAddPostRequestAction
    | TEditPostRequestAction
    | TDeletePostRequestAction
    | TRegProfileAction
    | TUserLoginAction
    | TEditProfileAction
    | TFormSubmitAction;
