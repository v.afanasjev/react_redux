import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { requestWorker } from './request-worker';

describe('requestWorker', () => {
    test('request success', () => {
        const data = { hello: 'world' };
        const fetchApi = () => data;
        return expectSaga(requestWorker, fetchApi, 'Mock')
            .put({ type: 'loading/setLoading', payload: true })
            .provide([[matchers.call.fn(fetchApi), data]])
            .put({ type: 'loading/setLoading', payload: false })
            .returns({ hello: 'world' })
            .run();
    });

    test('request fail', () => {
        const data = null;
        const fetchApi = () => data;
        return expectSaga(requestWorker, fetchApi, 'Mock')
            .put({ type: 'loading/setLoading', payload: true })
            .provide([[matchers.call.fn(fetchApi), throwError(new Error('error'))]])
            .put({ type: 'loading/setLoading', payload: false })
            .returns(undefined)
            .run();
    });
});
