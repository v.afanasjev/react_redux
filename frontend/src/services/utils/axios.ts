import axiosRaw from 'axios';

export const axios = axiosRaw.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    timeout: 1000,
});
