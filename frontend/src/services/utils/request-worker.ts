import { AxiosError } from 'axios';
import { put, delay, call } from 'redux-saga/effects';
import { setLoading } from '../slices/loading-slice';
import { notificationError } from '../utils/notification';

const failMessage = 'request fail';
const delayTime = process.env.NODE_ENV === 'test' ? 0 : 300;

export function* requestWorker(fetchApi: () => unknown, errorTopic: string) {
    yield put(setLoading(true));
    try {
        const data: unknown = yield call(fetchApi);
        // loading imitation
        yield delay(delayTime);
        if (data) {
            yield put(setLoading(false));
            return data;
        } else {
            yield call(notificationError, `${errorTopic} ${failMessage}`);
        }
    } catch (err) {
        if (err instanceof AxiosError) {
            yield call(
                notificationError,
                err?.response?.data.message ?? `${errorTopic} ${failMessage}`
            );
            console.error(`${errorTopic} ${failMessage} `, err);
        }
    }
    yield put(setLoading(false));
}
