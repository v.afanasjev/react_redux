import { toast, ToastOptions } from 'react-toastify';

const defaultMessage = '🦄 Wow so easy!';
const options: ToastOptions = {
    position: 'bottom-right',
    autoClose: 2000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: 0,
    theme: 'colored',
    // may customize
    // style: { background: '#766852' }
};

export const notification = (message: string = defaultMessage) => toast(message, options);
export const notificationInfo = (message: string = defaultMessage) => toast.info(message, options);
export const notificationSuccess = (message: string = defaultMessage) =>
    toast.success(message, options);
export const notificationWarning = (message: string = defaultMessage) =>
    toast.warn(message, options);
export const notificationError = (message: string = defaultMessage) =>
    toast.error(message, options);
