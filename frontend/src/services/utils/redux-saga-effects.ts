import { select, Effect } from 'redux-saga/effects';
import type { TApplicationState } from '../types/application-state';

export type SagaGenerator<RT> = Generator<Effect<unknown>, RT, unknown>;

export function* selectTs<Args extends Array<unknown>, R>(
    selector: (state: TApplicationState, ...args: Args) => R,
    ...args: Args
): SagaGenerator<R> {
    // eslint-disable-next-line
    // @ts-expect-error
    return yield select(selector, ...args);
}
