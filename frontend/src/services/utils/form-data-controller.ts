class FormDataController {
    #formData: FormData | undefined;
    constructor() {
        this.#formData = undefined;
        this.getOrCreateFormData = this.getOrCreateFormData.bind(this);
        this.destroyFormData = this.destroyFormData.bind(this);
        this.appendToFormData = this.appendToFormData.bind(this);
    }

    getOrCreateFormData() {
        if (!this.#formData) {
            this.#formData = new FormData();
        }
        return this.#formData;
    }

    destroyFormData() {
        this.#formData = undefined;
    }

    appendToFormData(key: string, value: string | Blob) {
        this.#formData?.append(key, value);
    }
}

export const formDataController = new FormDataController();
