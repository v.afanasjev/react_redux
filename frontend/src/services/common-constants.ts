export const defaultSrc = '/static/images/avatar/1.jpg';

export const postsPerPage = 3;

export const modes = {
  editProfile: 'editProfile',
  createPost: 'createPost',
  editPost: 'editPost',
  deletePost: 'deletePost'
} as const;

export const forms = {
  editProfile: {
    formName: 'edit-profile-form' as const,
    formFields: {
      username: 'username',
      profile_picture: 'profile_picture'
    }
  },
  createEditPost: {
    formName: 'create-edit-post-form' as const,
    formFields: {
      title: 'title',
      content: 'content'
    }
  },
  auth: {
    formName: 'auth' as const,
    formFields: {
      username: 'username',
      email: 'email',
      password: 'password'
    }
  }
};

export const localStorageKeys = {
  accessToken: 'accessToken',
  loggedInUserId: 'loggedInUserId'
};
