import { getToken } from '../helpers/auth-helpers';
import { getPostsRequestPath } from '../helpers/prepare-to-request-helpers';
import { TGetPostsResponseData, TCommonResponse } from '../types/requests-types';
import { axios } from '../utils/axios';

export const getPostsRequest = async (
    userId: string,
    offset: number,
    limit: number
): Promise<TGetPostsResponseData | undefined> => {
    const result = await axios.get<TGetPostsResponseData>(
        getPostsRequestPath(userId, offset, limit)
    );
    return result.data;
};

export const addPostRequest = async (body: {
    title: string;
    content: string;
}): Promise<TCommonResponse | undefined> => {
    const result = await axios.post('posts/add', body, {
        headers: { Authorization: `Bearer ${getToken()}` },
    });
    return result.data;
};

export const editPostRequest = async (
    postId: string,
    body: {
        title: string;
        content: string;
    }
): Promise<TCommonResponse | undefined> => {
    const result = await axios.patch(`posts/${postId}`, body, {
        headers: { Authorization: `Bearer ${getToken()}` },
    });
    return result.data;
};

export const deletePostRequest = async (postId: string): Promise<TCommonResponse | undefined> => {
    const result = await axios.delete(`posts/${postId}`, {
        headers: { Authorization: `Bearer ${getToken()}` },
    });
    return result.data;
};
