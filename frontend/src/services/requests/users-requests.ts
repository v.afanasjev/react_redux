import { getToken } from '../helpers/auth-helpers';
import { TGetAllUsersResponseData, TGetUserResponseData } from '../types/requests-types';
import { axios } from '../utils/axios';

export const getAllUsersRequest = async (): Promise<TGetAllUsersResponseData | undefined> => {
    const result = await axios.get<TGetAllUsersResponseData>('/users');
    return result.data;
};

export const getOneUserRequest = async (
    userId: string
): Promise<TGetUserResponseData | undefined> => {
    const result = await axios.get<TGetUserResponseData>(`/users/${userId}`);
    return result.data;
};

export const updateUserProfileRequest = async (
    userId: string,
    formData: FormData
): Promise<TGetUserResponseData | undefined> => {
    const result = await axios.patch<TGetUserResponseData>(`/users/${userId}`, formData, {
        headers: {
            'content-type': 'multipart/form-data',
            Authorization: `Bearer ${getToken()}`,
        },
    });
    return result.data;
};
