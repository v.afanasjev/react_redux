import { TCommonResponse, TLoginResponse } from '../types/requests-types';
import { axios } from '../utils/axios';

export const registrationRequest = async (body: {
    username: string;
    email: string;
    password: string;
}): Promise<TCommonResponse | undefined> => {
    const result = await axios.post('users/reg', body);
    return result.data;
};

export const loginRequest = async (body: {
    email: string;
    password: string;
}): Promise<TLoginResponse | undefined> => {
    const result = await axios.post('users/login', body);
    return result.data;
};
