import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { loadingSliceName } from './constants';

const loadingSlice = createSlice({
    name: loadingSliceName,
    initialState: false,
    reducers: {
        setLoading: (state, action: PayloadAction<boolean>) => {
            return action.payload;
        },
    },
});

export const { setLoading } = loadingSlice.actions;

export type TSetLoadingAction = Pick<typeof setLoading, 'type'> & { payload: boolean };

export default loadingSlice.reducer;
