import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { postsSliceName } from './constants';
import { emptyArray } from '../utils/entity';
import type { TPost } from '../types/posts-types';

export type TPostsState = Readonly<{
    posts: ReadonlyArray<TPost>;
    page: number;
    offset: number;
    total: number;
}>;

const initialState = {
    posts: emptyArray as unknown as ReadonlyArray<TPost>,
    page: 1,
    offset: 0,
    total: 0,
};

const postsSlice = createSlice({
    name: postsSliceName,
    initialState,
    reducers: {
        setPosts: (state, action: PayloadAction<ReadonlyArray<TPost>>) => {
            return { ...state, posts: action.payload };
        },
        setTotal: (state, action: PayloadAction<{ total: number }>) => {
            return { ...state, total: action.payload.total };
        },
        setPage: (state, action: PayloadAction<{ page: number; offset?: number }>) => {
            if (action.payload.offset) {
                return { ...state, page: action.payload.page, offset: action.payload.offset };
            }
            return { ...state, page: action.payload.page };
        },
    },
});

export const { setPosts, setTotal, setPage } = postsSlice.actions;

type TSetPostsAction = Pick<typeof setPosts, 'type'> & { payload: ReadonlyArray<TPost> };
type TSetTotalAction = Pick<typeof setTotal, 'type'> & { payload: { total: number } };
type TSetPageAction = Pick<typeof setPage, 'type'> & { payload: { page: number; offset?: number } };

export type TPostsActions = TSetPostsAction | TSetTotalAction | TSetPageAction;

export default postsSlice.reducer;
