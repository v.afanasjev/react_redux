import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { pagesSliceName, MAIN_PAGE } from './constants';
import type { REG_PAGE, LOGIN_PAGE, USER_PAGE } from './constants';

type TUserPage = typeof USER_PAGE;
type TUserPageWithId = `${TUserPage}${string}`;

export type TPage = typeof MAIN_PAGE | typeof LOGIN_PAGE | typeof REG_PAGE | TUserPageWithId;

export type TPagesState = Readonly<{
    current: TPage;
    target: TPage | undefined;
}>;

const initialState: TPagesState = {
    current: MAIN_PAGE,
    target: undefined,
};

const pagesSlice = createSlice({
    name: pagesSliceName,
    initialState,
    reducers: {
        setPage: (state, action: PayloadAction<TPage>) => {
            return { ...state, current: action.payload };
        },
        setTargetPage: (state, action: PayloadAction<TPage | undefined>) => {
            return { ...state, target: action.payload };
        },
    },
});

export const { setPage, setTargetPage } = pagesSlice.actions;

type TSetPageAction = Pick<typeof setPage, 'type'> & { payload: TPage };
type TSetTargetPageAction = Pick<typeof setTargetPage, 'type'> & { payload: TPage | undefined };

export type TPagesActions = TSetPageAction | TSetTargetPageAction;

export default pagesSlice.reducer;
