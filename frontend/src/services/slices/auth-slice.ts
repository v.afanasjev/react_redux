import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { authSliceName } from './constants';

const authSlice = createSlice({
    name: authSliceName,
    initialState: '',
    reducers: {
        setUserAuth: (state, action: PayloadAction<string>) => {
            return action.payload;
        },
    },
});

export const { setUserAuth } = authSlice.actions;

export type TSetUserAuthAction = Pick<typeof setUserAuth, 'type'> & { payload: string };

export default authSlice.reducer;
