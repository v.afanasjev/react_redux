import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { modalSliceName } from './constants';
import { modes } from '../common-constants';

type TEditPost = typeof modes.editPost;
type TEditPostWithId = `${TEditPost}${string}`;
type TDeletePost = typeof modes.deletePost;
type TDeletePostWithId = `${TDeletePost}${string}`;

export type TModalMode =
    | Exclude<keyof typeof modes, TEditPost | TDeletePost>
    | TEditPostWithId
    | TDeletePostWithId;

export type TModalState = Readonly<{
    isOpen: boolean;
    mode: TModalMode;
}>;

const initialState: TModalState = {
    isOpen: false,
    mode: modes.createPost,
};

const modalSlice = createSlice({
    name: modalSliceName,
    initialState,
    reducers: {
        toggleModal: (state, action: PayloadAction<boolean>) => {
            return { ...state, isOpen: action.payload };
        },
        changeMode: (state, action: PayloadAction<TModalMode>) => {
            return { ...state, mode: action.payload };
        },
    },
});

export const { toggleModal, changeMode } = modalSlice.actions;

type TToggleModalAction = Pick<typeof toggleModal, 'type'> & { payload: boolean };
type TChangeModeAction = Pick<typeof changeMode, 'type'> & { payload: TModalMode };

export type TModalActions = TToggleModalAction | TChangeModeAction;

export default modalSlice.reducer;
