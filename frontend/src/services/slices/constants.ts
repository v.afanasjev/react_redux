export const MAIN_PAGE = 'main';
export const LOGIN_PAGE = 'login';
export const REG_PAGE = 'reg';
export const USER_PAGE = 'user';

export const pagesSliceName = 'pages';
export const usersSliceName = 'users';
export const postsSliceName = 'posts';
export const loadingSliceName = 'loading';
export const modalSliceName = 'modal';
export const authSliceName = 'auth';
