import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { usersSliceName } from './constants';
import { emptyArray } from '../utils/entity';
import type { TUser } from '../types/users-types';

export type TUsersState = ReadonlyArray<TUser>;

const initialState = emptyArray as unknown as TUsersState;

const usersSlice = createSlice({
    name: usersSliceName,
    initialState,
    reducers: {
        setUsers: (state, action: PayloadAction<TUsersState>) => {
            return action.payload;
        },
    },
});

export const { setUsers } = usersSlice.actions;

type TSetUsersAction = Pick<typeof setUsers, 'type'> & { payload: TUsersState };

export type TPagesActions = TSetUsersAction;

export default usersSlice.reducer;
