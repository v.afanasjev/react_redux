import { CombinedState, Reducer, combineReducers } from '@reduxjs/toolkit';
import { reducer as reduxFormReducer } from 'redux-form';
import type { TApplicationAction } from '../types/application-action';
import type { TApplicationState } from '../types/application-state';
import {
    authSliceName,
    loadingSliceName,
    modalSliceName,
    pagesSliceName,
    postsSliceName,
    usersSliceName,
} from './constants';
import pagesReducer from './pages-slice';
import usersReducer from './users-slice';
import postsReducer from './posts-slice';
import loadingReducer from './loading-slice';
import modalReducer from './modal-slice';
import authReducer from './auth-slice';

export const rootReducer = combineReducers({
    [authSliceName]: authReducer,
    [pagesSliceName]: pagesReducer,
    [usersSliceName]: usersReducer,
    [postsSliceName]: postsReducer,
    [loadingSliceName]: loadingReducer,
    [modalSliceName]: modalReducer,
    form: reduxFormReducer,
}) as Reducer<CombinedState<TApplicationState>, TApplicationAction>;
