import React from 'react';
import { ThreeCircles } from 'react-loader-spinner';

const wrapperStyle = { position: 'absolute', left: '50%', top: '50%' };

export const Loader = ({ size = 80, color = '#4fa94d' }: { size?: number; color?: string }) => (
    <ThreeCircles
        height={size}
        width={size}
        color={color}
        wrapperStyle={wrapperStyle}
        wrapperClass=""
        visible={true}
        ariaLabel="three-circles-rotating"
        outerCircleColor=""
        innerCircleColor=""
        middleCircleColor="green"
    />
);
