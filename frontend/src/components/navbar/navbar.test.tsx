import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { Navbar } from './index';
import { useSelector } from '../../services/hooks';

const loggedInStateMock = {
    pages: { current: '' },
    auth: 'auth',
};
const unLoggedInStateMock = {
    pages: { current: '' },
    auth: '',
};

const setState = (state: unknown) => {
    (useSelector as jest.Mock<any, any>).mockImplementation((callback) => callback(state));
};

const mockText = 'test-link';
const regExp = new RegExp(mockText, 'i');
jest.mock('react-router-dom', () => ({
    Outlet: () => null,
    Link: () => <a>{mockText}</a>,
}));

const mockedDispatch = jest.fn();
const onLogout = () => {
    setState(unLoggedInStateMock);
    mockedDispatch();
};
jest.mock('../../services/hooks', () => ({
    useDispatch: () => onLogout,
    useSelector: jest.fn(),
}));

describe('navbar component', () => {
    beforeEach(() => setState(loggedInStateMock));
    test('isLoggedIn=true', () => {
        const screen = render(<Navbar />);
        const links = screen.getAllByText(regExp);

        expect(links).toBeDefined();
        expect(links.length).toEqual(1);
    });

    test('isLoggedIn=false', () => {
        setState(unLoggedInStateMock);
        const screen = render(<Navbar />);
        const links = screen.getAllByText(regExp);

        expect(links).toBeDefined();
        expect(links.length).toEqual(3);
    });

    test('logout click', () => {
        let screen = render(<Navbar />);
        let links = screen.getAllByText(regExp);

        fireEvent.click(screen.getByText(/logout/i));
        // now 3 more links will be rendered
        screen = render(<Navbar />);
        links = screen.getAllByText(regExp);

        expect(mockedDispatch).toBeCalledTimes(1);
        expect(links).toBeDefined();
        expect(links.length).toEqual(4);
    });
});
