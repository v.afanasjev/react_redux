import React from 'react';

import { Link, Outlet } from 'react-router-dom';
import { Box, Button } from '@mui/material';

import { useDispatch, useSelector } from '../../services/hooks';
import { authSelector, pagesSelector } from '../../services/selectors';
import { LOGIN_PAGE, MAIN_PAGE, REG_PAGE } from '../../services/slices/constants';
import { FlexBox } from '../containers';
import { setUserAuth } from '../../services/slices/auth-slice';

import './navbar.css';

export const Navbar = () => {
    const currentPage = useSelector((state) => pagesSelector(state).current);
    const isLoggedIn = useSelector((state) => Boolean(authSelector(state)));
    const dispatch = useDispatch();

    const onLogout = () => {
        dispatch(setUserAuth(''));
    };
    return (
        <Box className="navbar-container" padding={2}>
            <nav>
                <FlexBox justifyContent="space-between">
                    <ul>
                        <li>
                            <Link className={currentPage === MAIN_PAGE ? 'active-tab' : ''} to="/">
                                Main
                            </Link>
                        </li>

                        {!isLoggedIn && (
                            <>
                                <li>
                                    <Link
                                        className={currentPage === REG_PAGE ? 'active-tab' : ''}
                                        to="/reg"
                                    >
                                        Registration
                                    </Link>
                                </li>
                                <li>
                                    <Link
                                        className={currentPage === LOGIN_PAGE ? 'active-tab' : ''}
                                        to="/login"
                                    >
                                        Login
                                    </Link>
                                </li>
                            </>
                        )}
                    </ul>
                    {isLoggedIn && (
                        <Button variant="outlined" id="logout-btn" onClick={onLogout}>
                            logout
                        </Button>
                    )}
                </FlexBox>
            </nav>

            <Outlet />
        </Box>
    );
};
