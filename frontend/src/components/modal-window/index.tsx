import React from 'react';
import Modal from 'react-modal';
import { checkIsModalWindowLarge } from '../../services/helpers/prepare-to-render-helpers';

import { useSelector, useDispatch } from '../../services/hooks';
import { modalSelector } from '../../services/selectors';
import { type TModalMode, toggleModal } from '../../services/slices/modal-slice';
import { FormControls } from '../forms/form-controls';
import { ModalContent } from './modal-content';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        background: 'antiquewhite',
    },
    overlay: {
        backgroundColor: 'rgb(47, 79, 79, 0.75)',
    },
};

const getStyle = (mode: TModalMode) => ({
    ...customStyles,
    content: {
        ...customStyles.content,
        ...(checkIsModalWindowLarge(mode) && { width: '80%' }),
    },
});

export const ModalWindow = () => {
    const isOpen = useSelector((state) => modalSelector(state).isOpen);
    const mode = useSelector((state) => modalSelector(state).mode);
    const dispatch = useDispatch();
    const closeModal = () => dispatch(toggleModal(false));

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={closeModal}
            style={getStyle(mode)}
            contentLabel="Example Modal"
            ariaHideApp={false}
            shouldCloseOnOverlayClick={false}
        >
            <ModalContent />
            <FormControls />
        </Modal>
    );
};
