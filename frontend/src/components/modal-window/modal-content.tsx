import React from 'react';

import { Typography, Box } from '@mui/material';

import { modes } from '../../services/common-constants';
import { useSelector } from '../../services/hooks';
import { modalSelector } from '../../services/selectors';
import { CreateEditPostForm } from '../forms/create-edit-post-form';
import { EditProfileForm } from '../forms/edit-profile-form';

const questionText = 'Are you sure \n what you want to delete \n the post';

const { createPost, editPost, deletePost } = modes;

export const ModalContent = () => {
    const mode = useSelector((state) => modalSelector(state).mode);

    if (mode.includes(editPost) || mode === createPost) {
        return <CreateEditPostForm />;
    }
    if (mode.includes(deletePost)) {
        return (
            <Box whiteSpace="pre-line" textAlign="center">
                <Typography paragraph variant="h4">
                    {questionText}
                </Typography>
            </Box>
        );
    }
    return <EditProfileForm />;
};
