import React from 'react';

import { BaseFieldProps, WrappedFieldProps } from 'redux-form';
import { TextField, TextareaAutosize } from '@mui/material';

import { sanitizeWithCapitalize, sanitize } from '../../services/helpers/validation';
import { forms } from '../../services/common-constants';

export const renderTextField = ({
    input,
    meta: { touched, invalid, error },
    ...custom
}: BaseFieldProps & WrappedFieldProps) => (
    <TextField
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        value={sanitizeWithCapitalize(input.value)}
        {...custom}
    />
);

export const renderContentField = ({
    input,
    meta: { touched, invalid, error },
    ...custom
}: any) => (
    <TextareaAutosize
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        value={sanitizeWithCapitalize(input.value)}
        {...custom}
    />
);

const { formFields } = forms.auth;
export const authRenderTextField = ({
    input,
    meta: { touched, invalid, error },
    ...custom
}: BaseFieldProps & WrappedFieldProps) => (
    <TextField
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        value={
            input.name === formFields.username
                ? sanitizeWithCapitalize(input.value)
                : sanitize(input.value)
        }
        {...custom}
    />
);
