import React from 'react';

import { reduxForm } from 'redux-form';

import { usersDataSelector } from '../../../services/selectors';
import { useSelector } from '../../../services/hooks';
import { forms } from '../../../services/common-constants';
import { formValidate } from '../../../services/helpers/validation';
import { ProfileFormComponent } from './edit-profile-form-component';

const { formName, formFields } = forms.editProfile;

const ReduxProfileForm = reduxForm({
    form: formName,
    validate: formValidate(formFields),
})(ProfileFormComponent);

export const EditProfileForm = () => {
    const [{ username }] = useSelector((state) => usersDataSelector(state));
    return <ReduxProfileForm initialValues={{ username }} />;
};
