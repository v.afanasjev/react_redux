import React, { useState, useEffect, useRef, SyntheticEvent } from 'react';

import { Field, arrayPush } from 'redux-form';
import { InputLabel, Input, Avatar, Box } from '@mui/material';

import { formSelector, usersDataSelector } from '../../../services/selectors';
import { useSelector, useDispatch } from '../../../services/hooks';
import { formSubmit } from '../../../services/sagas/actions';
import { defaultSrc, forms } from '../../../services/common-constants';
import { getAvatarSrc } from '../../../services/helpers/prepare-to-render-helpers';
import { formDataController } from '../../../services/utils/form-data-controller';
import { FlexBox } from '../../containers';
import { emptyArray } from '../../../services/utils/entity';
import { renderTextField } from '../fields-renderers';

const { formName, formFields } = forms.editProfile;

export const ProfileFormComponent = () => {
    const [{ username, profile_picture }] = useSelector((state) => usersDataSelector(state));
    const [image] = useSelector(
        (state) => formSelector(formName)(state).values?.profile_picture ?? emptyArray
    );
    const [resetFileInput, setResetFileInput] = useState(false);
    const dispatch = useDispatch();
    const mountedRef = useRef(false);

    const handleSubmit = (e: SyntheticEvent) => {
        e.preventDefault();
        dispatch(formSubmit());
    };

    const onImageChange = (event: any) => {
        if (event.target.files?.[0]) {
            const image: File = event.target.files[0];
            const imageUrl = URL.createObjectURL(image);
            formDataController.getOrCreateFormData();
            formDataController.appendToFormData(formFields.profile_picture, image);
            dispatch(arrayPush(formName, formFields.profile_picture, imageUrl));
        }
    };

    useEffect(() => {
        if (mountedRef.current) {
            if (!image) {
                setResetFileInput(true);
                setTimeout(setResetFileInput, 1, false);
            }
        }
    }, [image]);

    useEffect(() => {
        mountedRef.current = true;
        return () => {
            formDataController.destroyFormData();
        };
    }, []);

    return (
        <form onSubmit={handleSubmit}>
            <FlexBox justifyContent="center">
                <Avatar
                    id="big-avatar"
                    alt={username}
                    src={image ?? getAvatarSrc(profile_picture) ?? defaultSrc}
                />
            </FlexBox>

            <Box marginBottom={3}>
                <Field
                    name={formFields.username}
                    component={renderTextField}
                    label="Name"
                    type="text"
                    variant="standard"
                    style={{ width: '80%' }}
                />
            </Box>
            <InputLabel>Profile Picture</InputLabel>
            <Box marginBottom={3}>
                {!resetFileInput && (
                    <Input
                        name={formFields.profile_picture}
                        type="file"
                        disableUnderline
                        onChange={onImageChange}
                    />
                )}
            </Box>
        </form>
    );
};
