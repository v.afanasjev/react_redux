import React from 'react';
import { reset, isPristine, isInvalid } from 'redux-form';

import Button from '@mui/material/Button';
import Box from '@mui/material/Box';

import { useSelector, useDispatch } from '../../services/hooks';
import { isUserPageSelector, modalSelector } from '../../services/selectors';
import { toggleModal } from '../../services/slices/modal-slice';
import { formSubmit } from '../../services/sagas/actions';
import { FlexBox } from '../containers';
import { forms, modes } from '../../services/common-constants';
import {
    getCurrentFormName,
    checkIsModalWindowLarge,
} from '../../services/helpers/prepare-to-render-helpers';

export const FormControls = () => {
    const isUserPage = useSelector(isUserPageSelector);
    const mode = useSelector((state) => modalSelector(state).mode);
    const isModalWindowLarge = checkIsModalWindowLarge(mode);

    const formName = isUserPage ? getCurrentFormName(mode) : forms.auth.formName;
    const pristine = useSelector((state) => isPristine(formName)(state));
    const invalid = useSelector((state) => isInvalid(formName)(state));
    const dispatch = useDispatch();

    const resetAction = reset(formName);
    const onReset = () => dispatch(resetAction);

    const onClose = () => dispatch(toggleModal(false));

    const onClick = () => dispatch(formSubmit());

    const isConfirmMode = isUserPage && mode.includes(modes.deletePost);

    const disabled = !isConfirmMode && (pristine || invalid);

    return (
        <FlexBox justifyContent={isModalWindowLarge ? 'end' : 'center'}>
            {!isConfirmMode && (
                <Box marginRight={3}>
                    <Button
                        color="secondary"
                        variant="contained"
                        disabled={disabled}
                        onClick={onReset}
                    >
                        Reset changes
                    </Button>
                </Box>
            )}
            <Box marginRight={3}>
                <Button color="success" variant="contained" disabled={disabled} onClick={onClick}>
                    {isConfirmMode ? 'Yes' : 'Submit'}
                </Button>
            </Box>
            {isUserPage && (
                <Button color="error" variant="contained" onClick={onClose}>
                    {isConfirmMode ? 'No' : 'Close'}
                </Button>
            )}
        </FlexBox>
    );
};
