import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { useSelector } from '../../services/hooks';
import { FormControls } from './form-controls';
import { USER_PAGE, LOGIN_PAGE } from '../../services/slices/constants';
import { modes } from '../../services/common-constants';

const resetBtnText = 'Reset changes';
const getRegExp = (str: string) => new RegExp(str, 'i');

const mockedDispatch = jest.fn();
jest.mock('../../services/hooks', () => ({
    useDispatch: () => mockedDispatch,
    useSelector: jest.fn(),
}));

const mockedValues = {
    email: 'mock-email',
    password: 'mock-password',
};

const mockState = (page: string, mode: string, isButtonsDisabled?: boolean) => ({
    pages: { current: page },
    modal: { mode },
    form: {
        auth: {
            values: isButtonsDisabled ? mockedValues : {},
        },
    },
});
const setState = (state: unknown) => {
    (useSelector as jest.Mock<any, any>).mockImplementation((callback) => callback(state));
};

describe('modal window form buttons', () => {
    test('rendered three buttons - reset and with standard labels', () => {
        setState(mockState(USER_PAGE, modes.createPost));
        const screen = render(<FormControls />);

        const resetBtn = screen.getByText(getRegExp(resetBtnText));
        // standatd labels
        const submitBtn = screen.getByText(getRegExp('Submit'));
        const closeBtn = screen.getByText(getRegExp('Close'));
        // confirm labels
        const agreeBtn = screen.queryByText(getRegExp('Yes'));
        const disagreeBtn = screen.queryByText(getRegExp('No'));

        expect(resetBtn).toBeDefined();
        // standatd labels
        expect(submitBtn).toBeDefined();
        expect(closeBtn).toBeDefined();
        // confirm labels
        expect(agreeBtn).toEqual(null);
        expect(disagreeBtn).toEqual(null);
    });

    test('rendered two buttons - with confirm mode labels', () => {
        setState(mockState(USER_PAGE, modes.deletePost));
        const screen = render(<FormControls />);

        const resetBtn = screen.queryByText(getRegExp(resetBtnText));
        // standatd labels
        const submitBtn = screen.queryByText(getRegExp('Submit'));
        const closeBtn = screen.queryByText(getRegExp('Close'));
        // confirm labels
        const agreeBtn = screen.getByText(getRegExp('Yes'));
        const disagreeBtn = screen.getByText(getRegExp('No'));

        expect(resetBtn).toEqual(null);
        // standatd labels
        expect(submitBtn).toEqual(null);
        expect(closeBtn).toEqual(null);
        // confirm labels
        expect(agreeBtn).toBeDefined();
        expect(disagreeBtn).toBeDefined();
    });

    test('rendered two buttons - reset and submit', () => {
        setState(mockState(LOGIN_PAGE, modes.createPost));
        const screen = render(<FormControls />);

        const resetBtn = screen.getByText(getRegExp(resetBtnText));
        // standatd labels
        const submitBtn = screen.getByText(getRegExp('Submit'));
        const closeBtn = screen.queryByText(getRegExp('Close'));
        // confirm labels
        const agreeBtn = screen.queryByText(getRegExp('Yes'));
        const disagreeBtn = screen.queryByText(getRegExp('No'));

        expect(resetBtn).toBeDefined();
        // standatd labels
        expect(submitBtn).toBeDefined();
        expect(closeBtn).toEqual(null);
        // confirm labels
        expect(agreeBtn).toEqual(null);
        expect(disagreeBtn).toEqual(null);
    });

    test('buttons disabled', () => {
        setState(mockState(LOGIN_PAGE, modes.createPost));
        const screen = render(<FormControls />);

        fireEvent.click(screen.getByText(getRegExp(resetBtnText)));
        fireEvent.click(screen.getByText(getRegExp('Submit')));
        expect(mockedDispatch).not.toBeCalled();
    });

    test('buttons clickable', () => {
        const isButtonsDisabled = true;
        setState(mockState(LOGIN_PAGE, modes.createPost, isButtonsDisabled));
        const screen = render(<FormControls />);
        // screen.debug(); // to check
        fireEvent.click(screen.getByText(getRegExp(resetBtnText)));
        fireEvent.click(screen.getByText(getRegExp('Submit')));
        expect(mockedDispatch).toBeCalledTimes(2);
        expect(mockedDispatch).toBeCalledWith({
            type: '@@redux-form/RESET',
            meta: { form: 'auth' },
        });
        expect(mockedDispatch).toBeCalledWith({ type: 'FORM_SUBMIT' });
    });
});
