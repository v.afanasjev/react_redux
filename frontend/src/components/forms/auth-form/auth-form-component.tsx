import React, { SyntheticEvent } from 'react';

import { Field } from 'redux-form';
import { Box, capitalize } from '@mui/material';

import { useDispatch, useSelector } from '../../../services/hooks';
import { formSubmit } from '../../../services/sagas/actions';
import { forms } from '../../../services/common-constants';
import { FlexBox } from '../../containers';
import { isLoginPageSelector } from '../../../services/selectors';
import { authRenderTextField } from '../fields-renderers';

const { formFields } = forms.auth;
const style = { width: '100%' };

const useFormFelds = () => {
    const isLoginPage = useSelector(isLoginPageSelector);
    const rawFields = Object.values(formFields);
    const fields = isLoginPage
        ? rawFields.filter((filed) => filed !== formFields.username)
        : rawFields;
    return { fields, isLoginPage };
};

export const AuthFormComponent = () => {
    const { fields, isLoginPage } = useFormFelds();
    const dispatch = useDispatch();

    const handleSubmit = (e: SyntheticEvent) => {
        e.preventDefault();
        dispatch(formSubmit());
    };
    return (
        <FlexBox width="inherit" justifyContent="center" padding={3}>
            <form data-testid="form" className="short-form" onSubmit={handleSubmit}>
                {fields.map((field, index) => (
                    <Box key={index} marginBottom={3}>
                        <Field
                            name={field}
                            component={authRenderTextField}
                            label={isLoginPage || index ? capitalize(field) : 'Name'}
                            type="text"
                            variant="standard"
                            style={style}
                        />
                    </Box>
                ))}
            </form>
        </FlexBox>
    );
};
