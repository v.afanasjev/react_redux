import { reduxForm } from 'redux-form';
import { forms } from '../../../services/common-constants';
import { formValidate } from '../../../services/helpers/validation';
import { AuthFormComponent } from './auth-form-component';

const { formName, formFields } = forms.auth;

export const AuthForm = reduxForm({
    form: formName,
    validate: formValidate(formFields),
})(AuthFormComponent);
