import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { fireEvent, render } from '@testing-library/react';
import { AuthForm } from '.';
import { REG_PAGE, LOGIN_PAGE } from '../../../services/slices/constants';

const getInitialState = (current) => ({
  pages: { current }
});

const middlewares = [];
const mockStore = configureStore(middlewares);

const getRegExp = (str) => new RegExp(str, 'i');

describe('auth form', () => {
  test('registration page - should contain 3 inputs', () => {
    const store = mockStore(getInitialState(REG_PAGE));
    const screen = render(<Provider store={store}><AuthForm /></Provider>);

    const nameInput = screen.getByLabelText(getRegExp('name'));
    const emailInput = screen.getByLabelText(getRegExp('email'));
    const passwordInput = screen.getByLabelText(getRegExp('password'));

    expect(nameInput).toBeDefined();
    expect(emailInput).toBeDefined();
    expect(passwordInput).toBeDefined();
  });

  test('login page - should contain 2 inputs', () => {
    const store = mockStore(getInitialState(LOGIN_PAGE));
    const screen = render(<Provider store={store}><AuthForm /></Provider>);

    const nameInput = screen.queryByLabelText(getRegExp('name'));
    const emailInput = screen.getByLabelText(getRegExp('email'));
    const passwordInput = screen.getByLabelText(getRegExp('password'));

    expect(nameInput).toEqual(null);
    expect(emailInput).toBeDefined();
    expect(passwordInput).toBeDefined();
  });

  test('auth-form dispatch changes to the store', () => {
    const store = mockStore(getInitialState(REG_PAGE));
    const screen = render(<Provider store={store}><AuthForm /></Provider>);

    const nameInput = screen.getByLabelText(getRegExp('name'));
    fireEvent.change(nameInput, { target: { value: 'new value' } });
    fireEvent.submit(screen.getByTestId('form'));

    const actions = store.getActions();
    expect(actions.find(action => action.type === '@@redux-form/CHANGE')).toBeDefined();
    expect(actions.find(action => action.type === 'FORM_SUBMIT')).toBeDefined();
  });
});
