import React from 'react';
import { reduxForm } from 'redux-form';

import { useSelector } from '../../../services/hooks';
import { forms } from '../../../services/common-constants';
import { formValidate } from '../../../services/helpers/validation';
import { getPostIdSelector, postsDataSelector } from '../../../services/selectors';
import { emptyObject } from '../../../services/utils/entity';
import { PostFormComponent } from './create-edit-post-form-component';

const { formName, formFields } = forms.createEditPost;

const ReduxPostFormComponent = reduxForm({
    form: formName,
    validate: formValidate(formFields),
})(PostFormComponent);

export const CreateEditPostForm = () => {
    const postId = useSelector(getPostIdSelector);
    const posts = useSelector((state) => postsDataSelector(state).posts);
    const currentPost = posts.find((post) => post.post_id === Number(postId));
    const initialValues = currentPost
        ? { title: currentPost.title, content: currentPost.content }
        : emptyObject;
    return <ReduxPostFormComponent initialValues={initialValues} />;
};
