import React, { SyntheticEvent } from 'react';

import { Field } from 'redux-form';
import { Box, capitalize } from '@mui/material';

import { useDispatch } from '../../../services/hooks';
import { formSubmit } from '../../../services/sagas/actions';
import { forms } from '../../../services/common-constants';
import { renderContentField, renderTextField } from '../fields-renderers';

const { formFields } = forms.createEditPost;

export const PostFormComponent = () => {
    const dispatch = useDispatch();

    const handleSubmit = (e: SyntheticEvent) => {
        e.preventDefault();
        dispatch(formSubmit());
    };

    return (
        <form onSubmit={handleSubmit}>
            <Box marginBottom={3}>
                <Field
                    name={formFields.title}
                    component={renderTextField}
                    label={capitalize(formFields.title)}
                    type="text"
                    variant="standard"
                    style={{ width: '100%' }}
                />
            </Box>
            <Box marginBottom={3}>
                <Field
                    name={formFields.content}
                    component={renderContentField}
                    aria-label="minimum height"
                    minRows={30}
                    placeholder={capitalize(formFields.content)}
                    style={{ width: '99.3%' }}
                />
            </Box>
        </form>
    );
};
