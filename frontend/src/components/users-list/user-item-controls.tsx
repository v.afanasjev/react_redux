import React from 'react';

import { Button, Box } from '@mui/material';

import { useDispatch } from '../../services/hooks';
import { toggleModal, changeMode } from '../../services/slices/modal-slice';
import { FlexBox } from '../containers';
import { modes } from '../../services/common-constants';

export const UserItemControls = () => {
    const dispatch = useDispatch();

    const openModal = () => dispatch(toggleModal(true));

    const onCreatePost = () => {
        dispatch(changeMode(modes.createPost));
        openModal();
    };

    const onEditProfile = () => {
        dispatch(changeMode(modes.editProfile));
        openModal();
    };
    return (
        <FlexBox flexDirection="column">
            <Box marginBottom={2}>
                <Button variant="contained" onClick={onCreatePost}>
                    create post
                </Button>
            </Box>
            <Button variant="contained" onClick={onEditProfile}>
                edit profile
            </Button>
        </FlexBox>
    );
};
