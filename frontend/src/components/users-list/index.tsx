import React from 'react';

import { useSelector } from '../../services/hooks';
import { usersDataSelector } from '../../services/selectors';
import { Pagination } from '../pagination';
import { UserItem } from './user-item';

import './users-list.css';

export const UsersList = ({
    onClick,
    isMainPage,
}: Readonly<{
    onClick?: (userId: number) => void;
    isMainPage?: boolean;
}>) => {
    const usersData = useSelector(usersDataSelector);

    return (
        <div className={isMainPage ? 'users-list clickable' : 'users-list'}>
            {usersData.map((userData, index) => (
                <UserItem key={index} isMainPage={isMainPage} onClick={onClick} {...userData} />
            ))}
            {!isMainPage && <Pagination />}
        </div>
    );
};
