import React from 'react';

import { Typography, Avatar, Box, capitalize } from '@mui/material';
import { Tooltip } from 'react-tooltip';

import { PostsList } from './posts-list';
import type { TUser } from '../../services/types/users-types';
import { defaultSrc } from '../../services/common-constants';
import { getAvatarSrc } from '../../services/helpers/prepare-to-render-helpers';

import { UserItemControls } from './user-item-controls';
import { FlexBox } from '../containers';
import { useSelector } from '../../services/hooks';
import { authSelector } from '../../services/selectors';

import './users-list.css';

const UserItemTooltip = () => (
    <Tooltip anchorSelect=".user-item" content="You can see all posts on this user's page" />
);

export const UserItem = ({
    posts,
    username,
    user_id,
    profile_picture,
    isMainPage,
    onClick: click,
}: TUser & { onClick?: (userId: number) => void; isMainPage?: boolean }) => {
    const loggedInUserId = useSelector(authSelector);
    const isLoggedIn = Boolean(loggedInUserId && +loggedInUserId === user_id);

    const onClick = () => click?.(user_id);

    return (
        <Box className={isMainPage ? 'user-item' : ''} onClick={onClick}>
            {isMainPage && <UserItemTooltip />}
            <FlexBox className="user-name" justifyContent="space-between">
                <FlexBox>
                    <Box marginRight={2}>
                        <Avatar
                            id={isMainPage ? '' : 'big-avatar'}
                            alt={username}
                            src={getAvatarSrc(profile_picture) ?? defaultSrc}
                        />
                    </Box>
                    <Typography variant="h5">{capitalize(username)}</Typography>
                </FlexBox>
                {!isMainPage && isLoggedIn ? <UserItemControls /> : null}
            </FlexBox>
            <Box marginTop={2}>
                <PostsList posts={posts} isMainPage={Boolean(isMainPage)} isLoggedIn={isLoggedIn} />
            </Box>
        </Box>
    );
};
