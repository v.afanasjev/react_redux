import React from 'react';

import { capitalize, Box, Typography, Button } from '@mui/material';

import { useDispatch } from '../../services/hooks';
import { dateFormatter } from '../../services/helpers/prepare-to-render-helpers';
import type { TPost } from '../../services/types/posts-types';
import { FlexBox } from '../containers';
import { changeMode, toggleModal } from '../../services/slices/modal-slice';
import { modes } from '../../services/common-constants';

export const PostItem = (props: TPost & { isMainPage: boolean; isLoggedIn: boolean }) => {
    const dispatch = useDispatch();

    const openModal = () => dispatch(toggleModal(true));

    const onEditPost = () => {
        dispatch(changeMode(`${modes.editPost}${props.post_id}`));
        openModal();
    };

    const onDeletePost = () => {
        dispatch(changeMode(`${modes.deletePost}${props.post_id}`));
        openModal();
    };
    return (
        <Box marginLeft={2} marginTop={2}>
            <FlexBox justifyContent="space-between" marginRight={2} marginBottom={2}>
                <Typography variant="h6">{capitalize(props.title)}</Typography>
                <Typography variant="overline" marginLeft={1}>
                    {`post created: ${dateFormatter(props.createdAt)}`}
                </Typography>
            </FlexBox>
            <FlexBox alignItems="start">
                <FlexBox flex={1} width="100%">
                    <Typography variant="body1" paragraph noWrap={props.isMainPage}>
                        {props.content}
                    </Typography>
                </FlexBox>
                {!props.isMainPage && props.isLoggedIn && (
                    <FlexBox flexDirection="column">
                        <Box marginLeft={2} marginBottom={2} width={90}>
                            <Button color="info" variant="contained" onClick={onEditPost} fullWidth>
                                edit
                            </Button>
                        </Box>
                        <Box marginLeft={2} width={90}>
                            <Button color="error" variant="contained" onClick={onDeletePost}>
                                delete
                            </Button>
                        </Box>
                    </FlexBox>
                )}
            </FlexBox>
        </Box>
    );
};
