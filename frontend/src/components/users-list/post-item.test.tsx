import React from 'react';
import renderer from 'react-test-renderer';
import { PostItem } from './post-item';
import mock from './mocked-data.json';

jest.mock('../../services/hooks');

describe('post-item component snapshots', () => {
    test('isMainPage=false and isLoggedIn=false', () => {
        const component = renderer.create(
            <PostItem isMainPage={false} isLoggedIn={false} {...mock}></PostItem>
        );
        expect(component.toJSON()).toMatchSnapshot();
    });

    test('isMainPage=true and isLoggedIn=false', () => {
        const component = renderer.create(
            <PostItem isMainPage isLoggedIn={false} {...mock}></PostItem>
        );
        expect(component.toJSON()).toMatchSnapshot();
    });

    test('isMainPage=false and isLoggedIn=true', () => {
        const component = renderer.create(
            <PostItem isMainPage={false} isLoggedIn {...mock}></PostItem>
        );
        expect(component.toJSON()).toMatchSnapshot();
    });

    test('isMainPage=true and isLoggedIn=true', () => {
        const component = renderer.create(<PostItem isMainPage isLoggedIn {...mock}></PostItem>);
        expect(component.toJSON()).toMatchSnapshot();
    });
});
