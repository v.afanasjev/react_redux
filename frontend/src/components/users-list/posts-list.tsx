import React from 'react';

import { useSelector } from '../../services/hooks';
import { postsDataSelector } from '../../services/selectors';
import type { TPost } from '../../services/types/posts-types';
import { emptyArray } from '../../services/utils/entity';
import { PostItem } from './post-item';

export const PostsList = ({
    posts: mainPosts = emptyArray as unknown as ReadonlyArray<TPost>,
    isMainPage,
    isLoggedIn,
}: {
    posts: ReadonlyArray<TPost> | undefined;
    isMainPage: boolean;
    isLoggedIn: boolean;
}) => {
    const userPosts = useSelector((state) => postsDataSelector(state).posts);
    const posts = isMainPage ? mainPosts : userPosts;

    return (
        <>
            {posts.map((post, index) => (
                <PostItem key={index} isMainPage={isMainPage} isLoggedIn={isLoggedIn} {...post} />
            ))}
        </>
    );
};
