import React from 'react';

import MuiPagination from '@mui/material/Pagination';

import { changePage } from '../services/sagas/actions';
import { useDispatch, useSelector } from '../services/hooks';
import { postsDataSelector } from '../services/selectors';
import { getPageCount } from '../services/helpers/prepare-to-render-helpers';

export const Pagination = () => {
    const total = useSelector((state) => postsDataSelector(state).total);
    const page = useSelector((state) => postsDataSelector(state).page);
    const dispatch = useDispatch();

    const handleChange = (e: unknown, page: number) => dispatch(changePage({ page }));

    return <MuiPagination count={getPageCount(total)} page={page} onChange={handleChange} />;
};
