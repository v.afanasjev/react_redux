import React from 'react';

import Box, { type BoxProps } from '@mui/material/Box';

export const FlexBox = (props: BoxProps) => (
    <Box display="flex" alignItems="center" {...props}>
        {props.children}
    </Box>
);
