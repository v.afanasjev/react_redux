import React from 'react';
import { render } from '@testing-library/react';
import { AppNavigator } from './app-navigator';
import { useSelector } from '../services/hooks';

const resetTargetPageAction = {
    payload: undefined,
    type: 'pages/setTargetPage',
};

const mockedDispatch = jest.fn();
jest.mock('../services/hooks', () => ({
    useDispatch: () => mockedDispatch,
    useSelector: jest.fn(),
}));

const mockedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
    useNavigate: () => mockedNavigate,
}));

const setState = (state: unknown) => {
    (useSelector as jest.Mock<any, any>).mockImplementation((callback) => callback(state));
};

describe('app navigation component', () => {
    afterAll(() => {
        (useSelector as jest.Mock<any, any>).mockClear();
    });
    test('navigate to target page', () => {
        const mockState = {
            pages: {
                target: 'target-page',
                current: 'current-page',
            },
        };
        setState(mockState);
        render(<AppNavigator />);

        expect(mockedDispatch).toBeCalledTimes(1);
        expect(mockedDispatch).toBeCalledWith(resetTargetPageAction);
        expect(mockedNavigate).toBeCalledWith(mockState.pages.target);
    });

    test('target page is user-page', () => {
        const mockState = {
            pages: {
                target: 'user-page',
                current: 'current-page',
            },
        };
        setState(mockState);
        render(<AppNavigator />);

        expect(mockedDispatch).toBeCalledTimes(2);
        expect(mockedDispatch).toBeCalledWith({
            payload: true,
            type: 'loading/setLoading',
        });
        expect(mockedDispatch).toBeCalledWith(resetTargetPageAction);
        expect(mockedNavigate).toBeCalledWith(mockState.pages.target);
    });

    test('target page equal current page', () => {
        const mockState = {
            pages: {
                target: 'current-page',
                current: 'current-page',
            },
        };
        setState(mockState);
        render(<AppNavigator />);

        expect(mockedDispatch).not.toBeCalled();
        expect(mockedNavigate).not.toBeCalled();
    });
});
