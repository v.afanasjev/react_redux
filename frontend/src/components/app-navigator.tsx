import React, { useEffect, useRef } from 'react';

import { useNavigate } from 'react-router-dom';

import { useDispatch, useSelector } from '../services/hooks';
import { pagesSelector } from '../services/selectors';
import { USER_PAGE } from '../services/slices/constants';
import { setLoading } from '../services/slices/loading-slice';
import { setTargetPage } from '../services/slices/pages-slice';

export const AppNavigator = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const currentPage = useSelector((state) => pagesSelector(state).current);
    const targetPage = useSelector((state) => pagesSelector(state).target);
    const currentPageRef = useRef(currentPage);
    currentPageRef.current = currentPage;

    useEffect(() => {
        if (targetPage && currentPageRef.current !== targetPage) {
            if (targetPage.includes(USER_PAGE)) {
                dispatch(setLoading(true));
            }
            dispatch(setTargetPage());
            navigate(targetPage);
        }
    }, [dispatch, navigate, targetPage]);

    return <></>;
};
