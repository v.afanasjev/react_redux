import * as React from 'react';
import { Routes, Route } from 'react-router-dom';

import { useDispatch } from './services/hooks';
import { Navbar } from './components/navbar';
import { LoginPage } from './pages/login-page';
import { RegistrationPage } from './pages/registration-page';
import { MainPage } from './pages/main-page';
import { ToastContainer } from 'react-toastify';
import { UserPage } from './pages/user-page';
import { ModalWindow } from './components/modal-window';
import { AppNavigator } from './components/app-navigator';
import { setUserAuth } from './services/slices/auth-slice';
import { getLoggedInUserId } from './services/helpers/auth-helpers';
import { LoaderPage } from './pages/loader-page';

import 'react-toastify/dist/ReactToastify.css';
import 'react-tooltip/dist/react-tooltip.css';

export default function App() {
    const dispatch = useDispatch();
    React.useEffect(() => {
        dispatch(setUserAuth(getLoggedInUserId()));
    }, [dispatch]);

    return (
        <>
            <Navbar />
            <Routes>
                <Route
                    index
                    element={
                        <LoaderPage>
                            <MainPage />
                        </LoaderPage>
                    }
                />
                <Route
                    path="reg"
                    element={
                        <LoaderPage>
                            <RegistrationPage />
                        </LoaderPage>
                    }
                />
                <Route
                    path="login"
                    element={
                        <LoaderPage>
                            <LoginPage />
                        </LoaderPage>
                    }
                />
                <Route
                    path="user/:userId"
                    element={
                        <LoaderPage>
                            <UserPage />
                        </LoaderPage>
                    }
                />

                <Route
                    path="*"
                    element={
                        <LoaderPage>
                            <MainPage />
                        </LoaderPage>
                    }
                />
            </Routes>
            <ToastContainer />
            <ModalWindow />
            <AppNavigator />
        </>
    );
}
