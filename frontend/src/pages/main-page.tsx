import React, { useCallback } from 'react';

import { useDispatch, useOnLoadPage } from '../services/hooks';
import { mainPageOnLoaded } from '../services/sagas/actions';
import { MAIN_PAGE } from '../services/slices/constants';
import { setPage, setTargetPage } from '../services/slices/pages-slice';
import { UsersList } from '../components/users-list';

export const MainPage = () => {
    const dispatch = useDispatch();
    useOnLoadPage([setPage(MAIN_PAGE), mainPageOnLoaded()]);

    const onClick = useCallback(
        (userId: number) => {
            dispatch(setTargetPage(`user/${userId}`));
        },
        [dispatch]
    );

    return <UsersList onClick={onClick} isMainPage />;
};
