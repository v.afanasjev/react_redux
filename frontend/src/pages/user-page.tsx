import React from 'react';
import { useParams } from 'react-router-dom';
import { UsersList } from '../components/users-list';
import { useOnLoadPage } from '../services/hooks';
import { userPageOnLoaded } from '../services/sagas/actions';
import { USER_PAGE } from '../services/slices/constants';
import { setPage } from '../services/slices/pages-slice';

export const UserPage = () => {
    const { userId } = useParams();
    useOnLoadPage([setPage(`${USER_PAGE}${userId ?? ''}`), userPageOnLoaded()]);

    return <UsersList />;
};
