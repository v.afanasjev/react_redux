import React from 'react';

import { useSelector } from '../services/hooks';
import { loadingSelector } from '../services/selectors';
import { Loader } from '../components/loader';

import './pages.css';

export const LoaderPage = ({ children }: { children: React.ReactNode }) => {
    const loading = useSelector(loadingSelector);

    return (
        <>
            {loading && (
                <div className="loader-page">
                    <Loader />
                </div>
            )}
            <div className={loading ? 'hidden' : ''}> {children}</div>
        </>
    );
};
