import React from 'react';

import { FlexBox } from '../components/containers';
import { AuthForm } from '../components/forms/auth-form';
import { FormControls } from '../components/forms/form-controls';
import { useOnLoadPage } from '../services/hooks';
import { LOGIN_PAGE } from '../services/slices/constants';
import { setPage } from '../services/slices/pages-slice';

export const LoginPage = () => {
    useOnLoadPage(setPage(LOGIN_PAGE));

    return (
        <FlexBox
            margin="auto"
            className="form-wrapper"
            padding={2}
            marginTop={10}
            flexDirection="column"
        >
            <AuthForm />
            <FormControls />
        </FlexBox>
    );
};
