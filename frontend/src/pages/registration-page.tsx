import React from 'react';

import { useOnLoadPage } from '../services/hooks';
import { REG_PAGE } from '../services/slices/constants';
import { setPage } from '../services/slices/pages-slice';
import { AuthForm } from '../components/forms/auth-form';
import { FlexBox } from '../components/containers';
import { FormControls } from '../components/forms/form-controls';

import './pages.css';

export const RegistrationPage = () => {
    useOnLoadPage(setPage(REG_PAGE));

    return (
        <FlexBox
            margin="auto"
            className="form-wrapper"
            padding={2}
            marginTop={10}
            flexDirection="column"
        >
            <AuthForm />
            <FormControls />
        </FlexBox>
    );
};
