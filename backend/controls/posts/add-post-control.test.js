const { validationResult } = require('express-validator');
const Posts = require('../../models/posts');
const addPostControl = require('./add-post-control');

const mockErrorMessage = 'mock-error-message';
const mockUserData = { user_id: 42 };
const body = {
    title: 'mock-title',
    content: 'mock-content',
};
const user = { dataValues: mockUserData };
const req = { body, user };

const mockRes = () => {
    const res = {};
    res.status = (code) => {
        res.code = code;
        return res;
    };
    res.json = (msg) => {
        res.msg = msg;
        return res;
    };
    return res;
};

const mockedAddPostControl = () => addPostControl(req, mockRes());

jest.mock('express-validator', () => ({
    validationResult: jest.fn(),
}));

describe('add post control', () => {
    beforeEach(() => {
        // request not have validation error
        validationResult.mockImplementation(() => ({ isEmpty: () => true }));
    });
    afterEach(() => {
        // restore the spy created with spyOn
        jest.restoreAllMocks();
    });

    test('request have validation error', async () => {
        validationResult.mockImplementation(() => ({ isEmpty: () => false }));
        const response = await mockedAddPostControl();
        expect(response.code).toEqual(400);
        expect(response.msg.message).toEqual('Create post invalid data');
    });
    test('addPostControl successfully adds post', async () => {
        const spy = jest.spyOn(Posts, 'create').mockImplementation(() => true);
        const response = await mockedAddPostControl();
        expect(spy).toHaveBeenCalled();
        expect(spy).toBeCalledWith({ ...body, ...mockUserData });
        expect(response.code).toEqual(200);
        expect(response.msg).toEqual({ success: true });
    });

    test('catch error', async () => {
        jest.spyOn(Posts, 'create').mockImplementation(() => {
            throw new Error(mockErrorMessage);
        });
        const response = await mockedAddPostControl();
        expect(response.code).toEqual(400);
        expect(response.msg.error).toEqual(mockErrorMessage);
    });
});
