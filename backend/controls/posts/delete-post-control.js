const Posts = require('../../models/posts');

const deletePostControl = async (req, res) => {
    try {
        await Posts.destroy({
            where: {
                post_id: req.params.id,
                user_id: req.user.dataValues.user_id,
            },
        });
        return res.status(200).json({ success: true });
    } catch (err) {
        return res.status(400).json({
            error: err.message,
        });
    }
};

module.exports = deletePostControl;
