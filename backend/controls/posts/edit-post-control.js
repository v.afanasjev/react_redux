const { validationResult } = require('express-validator');
const Posts = require('../../models/posts');

const editPostControl = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ message: 'Edit post invalid data', errors });
        }

        const post = await Posts.findOne({
            where: {
                post_id: req.params.id,
                user_id: req.user.dataValues.user_id,
            },
        });
        if (!post) {
            return res.status(404).json({ message: 'Post not found' });
        }
        await Posts.update(
            {
                title: decodeURI(req.body.title),
                content: decodeURI(req.body.content),
            },
            {
                where: {
                    post_id: req.params.id,
                    user_id: req.user.dataValues.user_id,
                },
            }
        );
        return res.status(200).json({ success: true });
    } catch (err) {
        return res.status(400).json({
            error: err.message,
        });
    }
};

module.exports = editPostControl;
