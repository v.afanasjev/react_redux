const { validationResult } = require('express-validator');
const Posts = require('../../models/posts');

const addPostControl = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ message: 'Create post invalid data', errors });
        }
        await Posts.create({
            user_id: req.user.dataValues?.user_id,
            title: decodeURI(req.body.title),
            content: decodeURI(req.body.content),
        });
        return res.status(200).json({ success: true });
    } catch (err) {
        return res.status(400).json({
            error: err.message,
        });
    }
};

module.exports = addPostControl;
