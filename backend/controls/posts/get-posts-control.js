const Posts = require('../../models/posts');

const getPostsControl = async (req, res) => {
    try {
        const posts = await Posts.findAll({
            offset: req.query.offset,
            limit: req.query.limit,
            order: [['createdAt', 'DESC']],
            where: { user_id: req.params.userId },
        });
        return res.status(200).json({ posts });
    } catch (err) {
        return res.status(400).json({
            error: err.message,
        });
    }
};

module.exports = getPostsControl;
