const Users = require('../../models/users');
const Posts = require('../../models/posts');

const getOneUserControl = async (req, res) => {
    try {
        const user = await Users.findOne({
            where: { user_id: req.params.id },
        });
        const { count } = await Posts.findAndCountAll({
            where: { user_id: req.params.id },
        });
        res.status(user ? 200 : 404).json({
            user: { ...user.dataValues, postsTotal: count },
        });
    } catch (err) {
        res.status(400).json({
            error: err.message,
        });
    }
};

module.exports = getOneUserControl;
