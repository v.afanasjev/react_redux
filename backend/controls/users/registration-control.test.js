const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const Users = require('../../models/users');
const registerUserControl = require('./registration-control');

const mockErrorMessage = 'mock-error-message';
const mockPassword = 'mock-password';
const body = {
    username: 'mock-username',
    email: 'mock-email',
    password: mockPassword,
};
const req = { body };

const mockRes = () => {
    const res = {};
    res.status = (code) => {
        res.code = code;
        return res;
    };
    res.json = (msg) => {
        res.msg = msg;
        return res;
    };
    return res;
};

const mockedRegisterUserControl = () => registerUserControl(req, mockRes());

jest.mock('express-validator', () => ({
    validationResult: jest.fn(),
}));

describe('user registration control', () => {
    beforeEach(() => {
        // request not have validation error
        validationResult.mockImplementation(() => ({ isEmpty: () => true }));
    });
    afterEach(() => {
        // restore the spy created with spyOn
        jest.restoreAllMocks();
    });
    test('request have validation error', async () => {
        validationResult.mockImplementation(() => ({ isEmpty: () => false }));
        const response = await mockedRegisterUserControl();
        expect(response.code).toEqual(400);
        expect(response.msg.message).toEqual('Registration invalid data');
    });

    test('candidate with this email already registered', async () => {
        jest.spyOn(Users, 'findOne').mockReturnValue(true);
        const response = await mockedRegisterUserControl();
        expect(response.code).toEqual(400);
        expect(response.msg.message).toEqual('User with this email already registered');
    });

    test('successful registration', async () => {
        jest.spyOn(Users, 'findOne').mockReturnValue(false);
        // not working with req.password
        jest.spyOn(bcrypt, 'hash').mockReturnValue(mockPassword);
        const spy = jest.spyOn(Users, 'create').mockImplementation(() => true);
        const response = await mockedRegisterUserControl();
        expect(spy).toHaveBeenCalled();
        expect(spy).toBeCalledWith(body);
        expect(response.code).toEqual(200);
        expect(response.msg).toEqual({ success: true });
    });

    test('catch error', async () => {
        jest.spyOn(Users, 'findOne').mockImplementation(() => {
            throw new Error(mockErrorMessage);
        });
        const response = await mockedRegisterUserControl();
        expect(response.code).toEqual(400);
        expect(response.msg.error).toEqual(mockErrorMessage);
    });
});
