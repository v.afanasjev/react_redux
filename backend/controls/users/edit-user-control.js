const { validationResult } = require('express-validator');
const Users = require('../../models/users');
const { sanitize } = require('../../helpers/validation');

const editUserControl = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ message: 'Edit profile invalid data', errors });
        }

        // check is the same user
        const loggedInUserId = req.user.dataValues.user_id;
        const { user_id } = await Users.findOne({
            where: { user_id: req.params.id },
        });
        if (user_id !== loggedInUserId) {
            return res.status(401).json({ message: 'User Unauthorized' });
        }
        const username = sanitize(req.body.username);
        const profile_picture = req.file?.path;
        const updateFields = profile_picture
            ? { username, profile_picture: `images/${req.file.filename}` }
            : { username };
        await Users.update(updateFields, { where: { user_id: req.params.id } });
        const user = await Users.findOne({
            where: { user_id: req.params.id },
        });
        return res.status(200).json({ success: true, user });
    } catch (err) {
        return res.status(400).json({
            error: err.message,
        });
    }
};

module.exports = editUserControl;
