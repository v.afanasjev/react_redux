const Users = require('../../models/users');
const Posts = require('../../models/posts');

const getAllUsersControl = async (req, res) => {
    try {
        const users = await Users.findAll({
            include: [
                {
                    model: Posts,
                    limit: 3,
                    order: [['createdAt', 'DESC']],
                },
            ],
        });
        return res.status(users ? 200 : 404).json({ users });
    } catch (err) {
        return res.status(400).json({
            error: err.message,
        });
    }
};

module.exports = getAllUsersControl;
