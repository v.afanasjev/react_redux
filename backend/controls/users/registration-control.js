const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const Users = require('../../models/users');

const saltRounds = 10;

const registrationControl = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ message: 'Registration invalid data', errors });
        }
        const email = decodeURI(req.body.email);
        const condidate = await Users.findOne({
            where: { email },
        });
        if (condidate) {
            return res.status(400).json({ message: 'User with this email already registered' });
        }

        const hashedPassword = await bcrypt.hash(req.body.password, saltRounds);

        await Users.create({
            username: req.body.username,
            email,
            password: hashedPassword,
        });
        return res.status(200).json({ success: true });
    } catch (err) {
        return res.status(400).json({
            error: err.message,
        });
    }
};

module.exports = registrationControl;
