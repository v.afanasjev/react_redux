const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const Users = require('../../models/users');

const generateAccessToken = (user_id) =>
    jwt.sign({ user_id }, process.env.PRIVATE_KEY, { expiresIn: '24h' });

const loginControl = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ message: 'Login invalid data', errors });
        }
        const user = await Users.findOne({
            where: { email: req.body.email },
        });
        if (!user) {
            return res.status(400).json({ message: 'User with this email not found' });
        }

        const isValidPassword = bcrypt.compare(req.body.password, user.password);
        if (!isValidPassword) {
            return res.status(400).json({ message: 'Entered incorrect password' });
        }
        const token = generateAccessToken(user.user_id);
        return res.status(200).json({ success: true, token, userId: user.user_id });
    } catch (err) {
        return res.status(400).json({
            error: err.message,
        });
    }
};

module.exports = loginControl;
