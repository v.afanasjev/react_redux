const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Users = require('../../models/users');
const loginControl = require('./login-control');

const mockErrorMessage = 'mock-error-message';
const mockToken = 'mock-token';
const body = {
    email: 'mock-email',
    password: 'mock-password',
};
const req = { body };

const mockRes = () => {
    const res = {};
    res.status = (code) => {
        res.code = code;
        return res;
    };
    res.json = (msg) => {
        res.msg = msg;
        return res;
    };
    return res;
};

const mockedLoginControl = () => loginControl(req, mockRes());

jest.mock('express-validator', () => ({
    validationResult: jest.fn(),
}));

describe('user login control', () => {
    beforeEach(() => {
        // request not have validation error
        validationResult.mockImplementation(() => ({ isEmpty: () => true }));
    });
    afterEach(() => {
        // restore the spy created with spyOn
        jest.restoreAllMocks();
    });
    test('request have validation error', async () => {
        validationResult.mockImplementation(() => ({ isEmpty: () => false }));
        const response = await mockedLoginControl();
        expect(response.code).toEqual(400);
        expect(response.msg.message).toEqual('Login invalid data');
    });

    test('candidate with this email not found', async () => {
        jest.spyOn(Users, 'findOne').mockReturnValue(false);
        const response = await mockedLoginControl();
        expect(response.code).toEqual(400);
        expect(response.msg.message).toEqual('User with this email not found');
    });

    test('incorrect password', async () => {
        jest.spyOn(Users, 'findOne').mockReturnValue(true);
        jest.spyOn(bcrypt, 'compare').mockReturnValue(false);
        const response = await mockedLoginControl();
        expect(response.code).toEqual(400);
        expect(response.msg.message).toEqual('Entered incorrect password');
    });

    test('successful login', async () => {
        jest.spyOn(Users, 'findOne').mockReturnValue(true);
        jest.spyOn(bcrypt, 'compare').mockReturnValue(true);
        jest.spyOn(jwt, 'sign').mockReturnValue(mockToken);
        const response = await mockedLoginControl();
        expect(response.code).toEqual(200);
        expect(response.msg).toEqual({ success: true, token: mockToken });
    });

    test('catch error', async () => {
        jest.spyOn(Users, 'findOne').mockImplementation(() => {
            throw new Error(mockErrorMessage);
        });
        const response = await mockedLoginControl();
        expect(response.code).toEqual(400);
        expect(response.msg.error).toEqual(mockErrorMessage);
    });
});
