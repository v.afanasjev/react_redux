module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2021: true,
        'jest/globals': true,
    },
    extends: 'airbnb-base',
    overrides: [],
    parserOptions: {
        ecmaVersion: 'latest',
    },
    plugins: ['jest'],
    rules: {
        semi: ['error', 'always'],
        indent: 0,
        quotes: ['error', 'single'],
        camelcase: 0,
        'consistent-return': 0,
        'import/no-extraneous-dependencies': 0,
        'implicit-arrow-linebreak': 0,
        'comma-dangle': 0,
        'no-use-before-define': 0,
    },
};
