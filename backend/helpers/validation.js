const removeExtraSpaces = (str, i = 0, res = '') => {
    if (i >= str.length) return res;
    if (str[i] === ' ' && str[i + 1] === ' ') {
        return removeExtraSpaces(str, i + 1, res);
    }
    const result = res + str[i];
    return removeExtraSpaces(str, i + 1, result);
};

const capitalize = (str) => str.replace(/^\w|\s\w/, (char) => char.toUpperCase());

const sanitize = (str) => capitalize(removeExtraSpaces(str).trim());

module.exports = { sanitize };
