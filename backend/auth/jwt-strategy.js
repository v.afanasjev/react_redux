const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const MockStrategy = require('passport-mock-strategy');
const { ExtractJwt } = require('passport-jwt');
const Users = require('../models/users');

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.PRIVATE_KEY;

const jwtStrategy = new JwtStrategy(opts, async (jwt_payload, done) => {
    try {
        const user = await Users.findOne({
            where: { user_id: jwt_payload.user_id },
        });
        if (user) {
            return done(null, user);
        }
        return done(null, false);
    } catch (err) {
        return done(err, false);
    }
});

const mockStrategy = new MockStrategy();

passport.use(process.env.NODE_ENV !== 'test' ? jwtStrategy : mockStrategy);
