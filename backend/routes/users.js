const express = require('express');
const { check, body } = require('express-validator');
const path = require('path');
const passport = require('passport');
const multer = require('multer');

const storage = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, path.resolve(__dirname, '../public/images/'));
    },
    filename(req, file, cb) {
        const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
        cb(null, `${req.params.id}-${uniqueSuffix}-${file.originalname}`);
    },
});

const upload = multer({ storage });

const router = express.Router();

const getAllUsersControl = require('../controls/users/get-all-users-control');
const registrationControl = require('../controls/users/registration-control');
const loginControl = require('../controls/users/login-control');
const getOneUserControl = require('../controls/users/get-one-user-control');
const editUserControl = require('../controls/users/edit-user-control');

const strategy = process.env.NODE_ENV === 'test' ? 'mock' : 'jwt';

router.get('/', getAllUsersControl);
router.post(
    '/reg',
    body('username', 'incorrect name').notEmpty().trim(),
    body('password', 'incorrect password').trim().isLength({ min: 4, max: 14 }),
    body('email', 'incorrect email').isEmail().normalizeEmail(),
    registrationControl
);
router.post(
    '/login',
    body('password', 'incorrect password').trim().isLength({ min: 4, max: 14 }),
    body('email', 'incorrect email').isEmail().normalizeEmail(),
    loginControl
);
router.get('/:id', getOneUserControl);
router.patch(
    '/:id',
    passport.authenticate(strategy, { session: false }),
    upload.single('profile_picture'),
    check('username', 'name should not be empty').notEmpty().trim().escape(),
    check('username', 'name should contain more than 3 symbols').isLength({ min: 4, max: 30 }),
    editUserControl
);

module.exports = router;
