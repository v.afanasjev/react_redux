const express = require('express');
const { body } = require('express-validator');
const passport = require('passport');

const router = express.Router();

const getPostsControl = require('../controls/posts/get-posts-control');
const addPostControl = require('../controls/posts/add-post-control');
const editPostControl = require('../controls/posts/edit-post-control');
const deletePostControl = require('../controls/posts/delete-post-control');

const strategy = process.env.NODE_ENV === 'test' ? 'mock' : 'jwt';

router.get('/:userId', getPostsControl);
router.post(
    '/add',
    passport.authenticate(strategy, { session: false }),
    body('title', 'title should not be empty').notEmpty().trim(),
    body('title', 'title should contain more than 3 symbols').isLength({ min: 4 }),
    body('content', 'content should not be empty').notEmpty().trim(),
    body('content', 'content should contain more than 3 symbols').isLength({ min: 4 }),
    addPostControl
);
router.patch(
    '/:id',
    passport.authenticate(strategy, { session: false }),
    body('title', 'title should not be empty').notEmpty().trim(),
    body('title', 'title should contain more than 3 symbols').isLength({ min: 4 }),
    body('content', 'content should not be empty').notEmpty().trim(),
    body('content', 'content should contain more than 3 symbols').isLength({ min: 4 }),
    editPostControl
);
router.delete('/:id', passport.authenticate(strategy, { session: false }), deletePostControl);

module.exports = router;
