const Sequilize = require('sequelize');

module.exports = new Sequilize('react_reduxDB', 'vyacheslav', '', {
    host: 'localhost',
    dialect: 'postgres',
    operatorsAliases: 0,
    pool: {
        max: 5,
        min: 0,
        acquire: 3000,
        idle: 10000,
    },
});
